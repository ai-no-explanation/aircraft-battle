export class Score{
    constructor(body_mian){
        this.body_mian = body_mian
        this.score = 0
        this.init()     // 初始化
    }

    init(){
        let scoreNode = document.createElement('div')
        scoreNode.className = 'score'          // 设置样式
        scoreNode.innerText = '积分：' + this.score

        this.body_mian.appendChild(scoreNode)  // 加入战场
        this.scoreNode = scoreNode
    }
}