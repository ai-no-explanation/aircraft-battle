import { isPZ } from "./tool.js"

export class Bullet{
    constructor(body_main, battlePlan, enemyList, scoreObj){
        this.body_main = body_main          // 主界面
        this.battlePlan = battlePlan        // 战斗机
        this.enemyList = enemyList  // 战机集合的对象
        this.attackHurt = 10        // 子弹的攻击力
        this.scoreObj = scoreObj          // 分数 的 对象
        this.init()
    }
    init(){
        // console.log(111, this.battlePlan)
        // 1.生成子弹
        this.showBullet()
        // 2.设置子弹位置
        this.bulletPosition()
        // 3.子弹移动----重点是与敌机的碰撞
        this.bulletMove()
    }

    showBullet(){
        let bulletNode = document.createElement('div')
        bulletNode.className = 'bullet'             // 设置样式
        this.body_main.appendChild(bulletNode)      // 加入场景主界面
        this.bullet = bulletNode
    }

    bulletPosition(){
        this.bullet.style.bottom = this.battlePlan.offsetHeight+ 'px'
        this.bullet.style.left = this.battlePlan.offsetLeft + this.battlePlan.offsetWidth/2 - this.bullet.offsetWidth/2 + 'px'
    }
    // 子弹移动
    bulletMove(){
        this.bulletTimer = setInterval(() => {     // 发射
            this.bullet.style.top = this.bullet.offsetTop - 15 +'px'    // 子弹进行移动

            // 检测与敌机相撞
            // console.log(this.enemyList,'1111111')       // 测试集合
            for(let enemy of this.enemyList){
                if(isPZ(this.bullet, enemy.enemyPlan)){   // 如果碰撞到则执行如下,运行成功
                    // 子弹与战机相撞，造成伤害，战机扣除生命值，并且子弹销毁
                    enemy.HP -= this.attackHurt     // 战机扣除生命值
                    // 子弹销毁----清除子弹前进的定时器
                    this.bulletDelete()
                    // 当敌方战机的生命值为0，或者说小于等于0时，进行战机销毁,并且积累分数
                    if(enemy.HP <= 0){
                        // 积累分数
                        this.showScore(enemy)
                        // 敌方战机销毁 --- 清除敌方战机前进的定时器---从集合中去除敌方战机
                        this.enemyDelete(enemy)
                    }
                }
            }

            if(this.bullet.offsetTop < - this.bullet.offsetHeight){  // 当子弹射出屏幕则销毁，已实现
                this.bullet.remove()                // 销毁子弹
                clearInterval(this.bulletTimer)     // 销毁子弹移动的定时器
            }

        }, 20);
    }

    bulletDelete(){
        clearInterval(this.bulletTimer)     // 关闭子弹前进的定时器
        // 播放子弹销毁的动画
        this.bullet.className = 'bullet_die'     // 子弹修改样式
        setTimeout(() => {                      // 0.2s后
            this.bullet.remove()                // 移除子弹
        }, 200);

    }

    showScore(enemy){
        this.scoreObj.score += enemy.score      // 累计分数
        // console.log(this.scoreObj.score)
        this.scoreObj.scoreNode.innerText = '积分：' + this.scoreObj.score  // 显示分数
    }

    enemyDelete(enemy){
        this.enemyList.delete(enemy)            // 集合中清除敌方战机---子弹不会碰撞尸体
        clearInterval(enemy.enemyTimer)         // 关闭敌方战机前进的定时器
        // 播放动画
        if(enemy.planeType === 'enemy-small'){      // 如果是小型战机
            let num1 = 0
            let showNum = 3         // 动画有几张图片
            let showTimeOut1 = 150   // 播动画的时间间隔
            let smallEnemyTimer = setInterval(() => {
                num1++
                if(num1 <= showNum){        // 有几张动画就播几张
                    enemy.enemyPlan.style.background = `url(images/plane1_die${num1}.png) no-repeat`
                } else {
                    clearInterval(smallEnemyTimer)      // 清除播动画的定时器
                    enemy.enemyPlan.remove()     
                }
            }, showTimeOut1);
        } else if(enemy.planeType === 'enemy-middle'){
            let num2 = 0
            let showNum = 4     // 中型战机动画有4张图片
            let showTimeOut2 = 150   // 播动画的时间间隔
            let middleEnemyTimer = setInterval(() => {
                num2++
                if(num2 <= showNum){
                    enemy.enemyPlan.style.background = `url(images/plane2_die${++num2}.png) no-repeat`
                } else {
                    clearInterval(middleEnemyTimer)      // 清除播动画的定时器
                    enemy.enemyPlan.remove() 
                }
            }, showTimeOut2);
        } else if(enemy.planeType === 'enemy-large'){
            let num3 = 0
            let showNum = 6         // 大型战机6张动画
            let showTimeOut3 = 150   // 播动画的时间间隔
            let largeEnemyTimer = setInterval(() => {
                num3++
                if(num3 <= showNum){
                    enemy.enemyPlan.style.background = `url(images/plane3_die${num3}.png) no-repeat`
                } else {
                    clearInterval(largeEnemyTimer)      // 清除播动画的定时器
                    enemy.enemyPlan.remove() 
                }
            }, showTimeOut3);
        } else {
            enemy.enemyPlan.remove()
        }


    }
}