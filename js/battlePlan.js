import animate from './animate.js'
export class BattlePlan{
    constructor(body_main){         // 我的战斗机类
        this.body_main = body_main
        this.HP = 5                 // 设定自己的战机生命值为5
        // console.log(111, this.body_main)
        this.init()
    }
    init(){
        // (插入)展示生命位置
        this.showBattlePlanHP()
        // 1.展示战斗机
        this.showBattlePlan()
        // 2.设置初始展示位置
        this.battlePlanPosition()
        // 3.设置移动
        this.battlePlanMove()
    }

    // 展示生命
    showBattlePlanHP(){
        let battlePlanHP = document.createElement('div')
        battlePlanHP.className = 'hp'        // 设置样式
        battlePlanHP.innerText = '生命:' + this.HP
        this.body_main.appendChild(battlePlanHP)      // 加入界面body
        this.battlePlanHP = battlePlanHP
    }

    showBattlePlan(){
        let battlePlanNode = document.createElement('div')
        battlePlanNode.className = 'my-warplain'        // 设置样式
        this.body_main.appendChild(battlePlanNode)      // 加入界面body
        this.battlePlan = battlePlanNode
    }
    // 设置初始位置，下方居中
    battlePlanPosition(){
        this.battlePlan.style.bottom = 0
        console.log(this.body_main.offsetWidth,123)
        this.battlePlan.style.left = (this.body_main.offsetWidth - this.battlePlan.offsetWidth)/2 + 'px'
    }
    // 设置我的战斗机的移动
    battlePlanMove(){
        document.onkeydown = (e) => {
            // console.log(e.key)
            let key = e.key
            let maxRight = this.body_main.offsetWidth - this.battlePlan.offsetWidth
            let nowPosition = this.battlePlan.offsetLeftth
            if(key==='ArrowRight'|| key==='d' || key==='D'){
                // console.log(111)
                nowPosition = this.battlePlan.offsetLeft + 50
                if(nowPosition > maxRight){
                    nowPosition = maxRight
                }
            }
            if(key==='ArrowLeft'|| key==='a' || key==='A'){
                // console.log(111)
                nowPosition = this.battlePlan.offsetLeft - 50
                if(nowPosition < 0){
                    nowPosition = 0
                }
            }
            // this.battlePlan.style.left = nowPosition +'px'
            animate(this.battlePlan, {left:nowPosition},5)
        }
    }
    // 我方战机死亡动画
    battleDelete(){
        let num1 = 0
        let showNum = 4         // 动画有几张图片
        let showTimeOut = 150   // 播动画的时间间隔
        let battleTimer = setInterval(() => {
            num1++
            if(num1 <= showNum){        // 有几张动画就播几张
                this.battlePlan.style.background = `url(images/me_die${num1}.png) no-repeat`
            } else {
                clearInterval(battleTimer)      // 清除播动画的定时器
                this.battlePlan.remove()             // 移除
            }
        }, showTimeOut);
    }
}