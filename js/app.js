
// 页面加载后出现等级选择
// 难度选择
// 点击选择难度后，
// 等级选择面版消失
// 出现欢迎面版
//  欢迎动画3秒后，欢迎动画消失
//  进入游戏界面
//  1. 背景运动
//  2. 战斗机出场
//  3. 炮弹发射
//  4. 敌机出现
//  5. 子弹与敌机碰撞
//      子弹爆炸 子弹消失
//       敌机受伤，敌机爆炸，敌机消失


// 第一步
// 引擎类 : Engine
    // 等级选择
    // 欢迎面版
    // 背景运动
// 第二步
    // 战斗机类: BattlePlan
    // 移动
    // 子弹类： Bullet
    // 发射 
    // 碰撞
    // 爆炸消失
    // 敌机 ： EnemyPlan
    // 小中大
    // 向下移动
    // 受伤 => 爆炸 =》 消失
    // 引入战斗机模块

import { Engine } from "./engine.js";
// import {BattlePlan} from "./battlePlan.js"
// import {Bullet} from "./bullet.js"
// import { EnemyPlan } from "./enemyPlan.js"
// import { getRand } from "./tool.js"

let body_main = document.querySelector('#body_main')    // 主界面
let options = document.querySelector('#options')        // 获取列表节点

// 导入引擎————游戏程序运行的入口，
//            主要负责游戏进入的界面，和相应的动画
new Engine(body_main, options)         // 传入主界面和列表节点，


