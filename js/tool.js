function isPrime(num){
	if(num <= 1) return false;
	for (var i = 2; i < num; i++) {
		if(num % i === 0) return false
	}
	return true;
}
// 求 min到max之间的随机整数
export function getRand(min,max){
//	return parseInt(Math.random()*(max-min+1)+min)
	return Math.floor(Math.random()*(max-min+1)+min)
}
// 获取随机num个字符的验证码
function getYZM(num){
	// 验证码的构成: 数字+字母
	// 1. 从 0-9 a-z A-Z
	// var str = "0123456789abc......"
	// ascii中 ： 48-57 65-90 97-122
	// 2. 随机取num次
	var yzm = "";
	for (var i = 0; i < num; i++) {
		// 取num次
		var rand = getRand(48,122)
		if((rand > 57 && rand < 65) || (rand > 90 && rand < 97)){
			// 这一次循环没有拼接到想要的字符，
			// 重新循环这一次
			i-- // 保证获取到的字符有num个
		}else{
			// 需要的字符
			yzm += String.fromCharCode(rand)
		}
	}
	return yzm
}
// 通过 key 获取对应查询参数的 value
function getSerachByKey(key){
	var searchList = location.search.substr(1).split("&");
	if(searchList.length === 0) {
		return ""
	}
	for (var i = 0; i < searchList.length; i++) {
		var list = searchList[i].split("=")
		if(list[0] === key){
			return list[1]
		}
	}
	return ""
}

// 格式化本地时间
function getLocaleDate(date){
	var weeks = ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"]
	var y = date.getFullYear()
	var m = date.getMonth()+1
	var d = date.getDate()
	var h = date.getHours()
	var f = date.getMinutes()
	var s = date.getSeconds()
	var w = date.getDay()	
	return `${y}年${db(m)}月${db(d)}日 ${db(h)}:${db(f)}:${db(s)} ${weeks[w]}`
}
function db(num){
	// 4 ==> 04
	return num < 10 ? 0+""+num : num
}

// 获取两个时间差的秒数
function getDifTime(start,end){
	return (end.getTime() - start.getTime())/1000
}

// 随机获取十六进制颜色
function getColor(){
	var ch = "0123456789abcdef";
	var color = "#";
	// 随机从ch中取6个字符
	// 将这6个字符拼接在 # 后面
	for (var i = 0; i < 6; i++) {
		var rand = getRand(0,15)
		color += ch[rand];
	}
	return color
}

// 获取浏览器计算后样式
function getStyle(ele,attr){
	// 判断是否支持 window.getComputedStyle
	if(window.getComputedStyle){
		// 支持
		return window.getComputedStyle(ele,null)[attr];
	}
	// ie8
	return ele.currentStyle[attr]
}
// 身份证号验证
function checkedCode(idCode){
	idCode = ""+idCode
	var idCodeReg = /^[0-9]\d{5}((19|20)\d{2}|2100)(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/
	
	if(idCodeReg.test(idCode)){
		// 通过基础验证后，再作平年闰年和大小月的验证
		// 获取年、月、日
		var y = parseInt(idCode.substr(6,4))
		var m = parseInt(idCode.substr(10,2))
		var d = parseInt(idCode.substr(12,2))
		
		switch(m){
			case 2:
				if(y % 4 === 0 && y % 100 != 0 || y % 400 === 0){
					if(d > 29) return false;
				}else{
					if(d > 28) return false;
				}
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				if(d > 30) return false
				
		}
		return true
	}
	
	return false;
}
export function isPZ(ele1,ele2){
	// 获取元素四边位置
	let r1 = ele1.getBoundingClientRect()
	let r2 = ele2.getBoundingClientRect()

	let r1L = r1.left
	let r1T = r1.top
	let r1R = r1.right
	let r1B = r1.bottom

	let r2L = r2.left
	let r2T = r2.top
	let r2R = r2.right
	let r2B = r2.bottom

	if(r1L > r2R || r1T > r2B || r1R < r2L || r1B < r2T){
		return false
	}

	return true

}