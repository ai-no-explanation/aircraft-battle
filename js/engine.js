import { BattlePlan } from "./battlePlan.js"
import { Bullet } from "./bullet.js"
import { EnemyPlan } from "./enemyPlan.js"
import { Score } from "./score.js"
import { getRand } from "./tool.js"

export class Engine{
    constructor(body_main, options){
        this.body_main = body_main      // 初始化类实例变量
        this.options = options
        this.enemyPlanList = new Set()
        this.init()
    }
    init(){     // 引擎初始化
        // 1.设置按钮事件
        console.log(this.options.children)
        let liList = this.options.children
        let liListLength = liList.length
        let _this = this    // 记录指向类实例的this指向,元素回调函数中this指向为元素
        for(let i=0;i<liListLength;i++){
            // 1.1 记录难度
            liList[i].onclick = function(){
                console.log(this.getAttribute('level'))
                _this.level = this.getAttribute('level')
            // 1.2 面板消失
                _this.options.remove()
            // 1.3 展示加载界面
                _this.showLoading()
            }
        }
    }

    // 加载界面
    showLoading(){
        // 1.背景运动起来   ok
        this.backScreenMove()
        // 2.显示飞机大战的logo--logo.png   ok
        this.showLogo()
        // 3.欢迎动画---loading1,2,3组成的动画
        this.showLoadingAnimate()
        // 4.3s后消除logo和动画，进入正式游戏
        setTimeout(() => {
            this.logoNode.remove()          // 消除logo
            this.loadingNode.remove()       // 消除动画节点
            clearInterval(this.loadTimer)   // 消除播放动画启动的定时器

            // 5.开始游戏
            this.playGame()
        }, 3000);

    }

    backScreenMove(){
        let speed = 0
        setInterval(() =>{
            this.body_main.style.backgroundPositionY = ++speed +'px'
            if(speed>=2000){
                speed = 0
            }
        }, 20)
    }

    showLogo(){
        let logoNode = document.createElement('div')    // 创造logo的节点
        logoNode.className = 'logo'             // 设置logo节点样式
        this.body_main.appendChild(logoNode)    // 将logo节点接入body分支
        this.logoNode = logoNode
    }
    // loading1\2\3.png
    showLoadingAnimate(){
        console.log('欢迎动画')
        // 添加背景
        let loadingNode = document.createElement('div')
        loadingNode.className = 'loading'
        this.body_main.appendChild(loadingNode)     // 添加到界面中
        loadingNode.style.background = `url(../images/loading1.png) no-repeat`
        // 添加加载动画
        this.loadingNode = loadingNode
        let num = 1
        this.loadTimer = setInterval(() => {
            loadingNode.style.background = `url(../images/loading${++num}.png) no-repeat`
            if(num === 3)
                num = 0
        }, 400);
    }


    // 开始游戏，要有我的战斗机、子弹、敌方战斗机
    playGame(){
        // （后来插入）加入积分
        let scoreObj = new Score(this.body_main)
        // 加入我的战斗机
        let battlePlanObj = new BattlePlan(this.body_main)
        // 加入子弹(我的战斗机)————子弹上膛
        // 子弹应该隔一段时间就发射一次，使用定时器
        // 最后要对子弹与敌方战斗机碰撞的检测
        let createBulletTimer = setInterval(() => {
            new Bullet(this.body_main, battlePlanObj.battlePlan, this.enemyPlanList, scoreObj)
        }, this.level);

        // 加入敌方战机,随机生成，随机出怪
        setInterval(()=>{
            new EnemyPlan(this.body_main, "enemy-small", this.enemyPlanList, battlePlanObj, createBulletTimer)
        }, getRand(400,800))
        setInterval(()=>{
            new EnemyPlan(this.body_main, "enemy-middle", this.enemyPlanList, battlePlanObj, createBulletTimer)
        }, getRand(800,1300))
        setInterval(()=>{
            new EnemyPlan(this.body_main, "enemy-large", this.enemyPlanList, battlePlanObj, createBulletTimer)
        }, getRand(3000,5000))
    }
}