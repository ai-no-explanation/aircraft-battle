import {getRand, isPZ} from './tool.js'

export class EnemyPlan{
    constructor(body_main, planeType, enemyList, battlePlanObj, createBulletTimer){
        this.body_main = body_main
        this.planeType = planeType
        this.enemyList = enemyList
        this.battlePlanObj = battlePlanObj      // 我方战机实例对象
        this.createBulletTimer = createBulletTimer
        // 添加敌方战机生命值和分数
        if(planeType === 'enemy-small'){
            this.HP = 10
            this.score = 10
        } else if(planeType === 'enemy-middle'){
            this.HP = 20
            this.score = 50
        } else if(planeType === 'enemy-large'){
            this.HP = 60
            this.score = 100
        } else {     // 预留给其他战机
            this.HP = 10
        }

        this.init()
    }

    init(){
        // 1.生成战斗机
        this.showEnemyPlan()
        // 2.设置初始位置
        this.enemyPlanPosition()
        // 3.战斗机移动
        this.enemyPlanMove()
    }

    showEnemyPlan(){
        let enemyPlanNode = document.createElement('div')
        enemyPlanNode.className = this.planeType         // 设置样式
        this.body_main.appendChild(enemyPlanNode)       // 加入主界面
        this.enemyPlan = enemyPlanNode
        this.enemyList.add(this)    // 将敌方战斗机的类实例对象加入集合
    }

    enemyPlanPosition(){
        // 地方战机的位置随机出现
        let randWidth = getRand(0, this.body_main.offsetWidth - this.enemyPlan.offsetWidth)
        this.enemyPlan.style.left = randWidth + 'px'      // 设置随机的位置
        this.enemyPlan.style.top = -this.enemyPlan.offsetHeight + 'px'
    }

    enemyPlanMove(){
        let speed_px = 10          // 设置战机的飞行速度
        if(this.planeType === 'enemy-middle'){
            speed_px = 4
        } else if(this.planeType === 'enemy-large'){
            speed_px = 2
        }

        this.enemyTimer = setInterval(() => {
            // 敌方战机移动
            this.enemyPlan.style.top = this.enemyPlan.offsetTop + speed_px + 'px'
            // animate(this.enemyPlan, {top:this.enemyPlan.offsetTop + 10}, 8)

            // 检测敌方战机是否与我方战机相撞
            if(isPZ(this.enemyPlan, this.battlePlanObj.battlePlan)){
                console.log('敌我双方战机相撞了')
                // 敌方战机死亡---先干掉定时器，再播动画,最后移除,不算消灭，不计分
                this.enemyDelete()

                // 我方战机的生命值降低,并且进行显示
                this.battlePlanObj.HP--
                this.battlePlanObj.battlePlanHP.innerText = '生命:' + this.battlePlanObj.HP
                if(this.battlePlanObj.HP <=0){
                    // 我方战机死亡---先干掉定时器，再播动画,最后移除
                    this.battlePlanObj.battleDelete()
                    // 战机死亡将不会再发射子弹，将发射子弹的定时器关闭
                    clearInterval(this.createBulletTimer)
                    // 展示游戏结束界面
                    this.showEnd()
                }
            }

            if(this.enemyPlan.offsetTop > this.body_main.offsetHeight-this.enemyPlan.offsetHeight){ // 销毁效果更明显
                this.enemyPlan.remove()         // 清除战斗机
                clearInterval(this.enemyTimer)       // 清除战斗机移动的定时器
                this.enemyList.delete(this.enemyPlan)   // 从集合中删除这架敌方飞机
            }
        }, 30);
    }

    enemyDelete(){
        this.enemyList.delete(this)   // 集合中删除敌机实例对象
        clearInterval(this.enemyTimer)          // 清除敌机前进的定时器，防止多次判定
        // 播动画 并移除
        if(this.planeType === 'enemy-small'){      // 如果是小型战机
            let num1 = 0
            let showNum = 3         // 动画有几张图片
            let showTimeOut1 = 150   // 播动画的时间间隔
            let smallEnemyTimer = setInterval(() => {
                num1++
                if(num1 <= showNum){        // 有几张动画就播几张
                    this.enemyPlan.style.background = `url(images/plane1_die${num1}.png) no-repeat`
                } else {
                    clearInterval(smallEnemyTimer)      // 清除播动画的定时器
                    this.enemyPlan.remove()             // 移除
                }
            }, showTimeOut1);
        } else if(this.planeType === 'enemy-middle'){
            let num1 = 0
            let showNum = 4         // 动画有几张图片
            let showTimeOut1 = 150   // 播动画的时间间隔
            let smallEnemyTimer = setInterval(() => {
                num1++
                if(num1 <= showNum){        // 有几张动画就播几张
                    this.enemyPlan.style.background = `url(images/plane2_die${num1}.png) no-repeat`
                } else {
                    clearInterval(smallEnemyTimer)      // 清除播动画的定时器
                    this.enemyPlan.remove()             // 移除
                }
            }, showTimeOut1);
        } else if(this.planeType === 'enemy-large'){
            let num1 = 0
            let showNum = 6         // 动画有几张图片
            let showTimeOut1 = 150   // 播动画的时间间隔
            let smallEnemyTimer = setInterval(() => {
                num1++
                if(num1 <= showNum){        // 有几张动画就播几张
                    this.enemyPlan.style.background = `url(images/plane3_die${num1}.png) no-repeat`
                } else {
                    clearInterval(smallEnemyTimer)      // 清除播动画的定时器
                    this.enemyPlan.remove()             // 移除
                }
            }, showTimeOut1);
        } else {
            this.enemyPlan.remove()
        }
    }

    showEnd(){
        // 展示文字
        let endNode = document.createElement('div')
        endNode.className = 'end'        // 设置样式
        endNode.innerText = '游戏结束'
        this.body_main.appendChild(endNode)      // 加入界面body
        this.endNode = endNode

        // 展示按钮
        let endBtnNode = document.createElement('div')
        endBtnNode.className = 'btn'        // 设置样式
        // endBtnNode.innerText = '游戏结束'
        this.body_main.appendChild(endBtnNode)      // 加入界面body
        let endButtonNode = document.createElement('button')
        endButtonNode.textContent = '重新开始'
        endBtnNode.appendChild(endButtonNode)
        endButtonNode.onclick = function(){
            location.href = location.href       // 重开
        }
    }
}