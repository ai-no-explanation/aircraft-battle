function getStyle(obj,attr){
	if(!!window.getComputedStyle){
		return window.getComputedStyle(obj)[attr];
	}else{
		return obj.currentStyle[attr];
	}
}

function animate(obj,param,ms,callBack){
	if(ms instanceof Function){
		callBack = ms;
		ms = undefined;
	}
	ms = ms || 10;
	clearInterval(obj.timer);
	obj.timer = setInterval(function(){
		var isOver = true; 
		for(var attr in param){
			if(attr != "zIndex"){
				var current = 0;
				if(attr === "opacity"){
					current = getStyle(obj,attr) * 100;
				}else{
					current = parseInt(getStyle(obj,attr));
				}
				var speed = (param[attr] - current) / 10;
				speed = speed > 0 ? Math.ceil(speed) : Math.floor(speed); // 将小数部分向上进1
				
				if(current != param[attr]){
					isOver = false; // false没有完成运行
					if(attr === "opacity"){
						// 透明度赋值需要缩小100倍
						obj.style[attr] = (current + speed) / 100;
					}else{
						obj.style[attr] = current + speed + "px";
					}
				}
			}else{
				obj.style[attr] = param[attr];
			}
		}
		if(isOver){
			clearInterval(obj.timer);
			if(!!callBack) callBack();
		}
	},ms)
}

export default animate